const select2Array = [
  "modal-filter-category-condition",
  "modal-filter-ip-condition",
  "modal-filter-term-condition",
  "modal-filter-details-condition",
  "modal-filter-single-date-condition",
  "modal-filter-range-date-condition",
];

function select2OnInit() {
  select2Array.forEach((elementId) => {
    $(`#${elementId}`).select2({
      dropdownParent: `#${elementId}-parent`,
      minimumResultsForSearch: "-1",
    });
  });
}

select2OnInit();
