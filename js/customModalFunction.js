function customOpenModal(modalID) {
    $("body").addClass("modal-open");
    $("body").css("overflow", "hidden");
    $("body").css("padding-right", "0px");
    $(`${modalID}`).addClass("show");
    $(`${modalID}`).css("display", "block");
    $(`${modalID}`).css("background-color", "rgba(0, 0, 0, 0.5)");
  
    // 綁定 goToTopScroll 事件
    // customScrollDetect(modalID);
    //
    $(".custom-goto-top").attr("onclick", `goToTop('${modalID}')`);
  
    // checkBoxDropdown()
  }
  
  function customCloseModal(modalID) {
    $("body").removeClass("modal-open");
    $("body").removeAttr("style");
    $(`${modalID}`).removeClass("show");
    $(`${modalID}`).removeAttr("style");
    $(`${modalID}`).unbind();
  
    // 取消綁定 goToTopScroll 事件
    // $(`${modalID}`).unbind();
    // Change scroll listener to window
    // goToTopScroll(window);
    $(".custom-goto-top").attr("onclick", `goToTop(window)`);
  }
  
  function customOpenSecondModal(currentModalID, newModalID) {
    $(`${currentModalID}`).removeClass("show");
    $(`${currentModalID}`).removeAttr("style");
  
    $(`${newModalID}`).addClass("show");
    $(`${newModalID}`).css("display", "block");
    $(`${newModalID}`).css("background-color", "rgba(0, 0, 0, 0.5)");
  
    // 取消綁定 goToTopScroll 事件
    $(`${currentModalID}`).unbind();
    // 綁定 goToTopScroll 事件
    // customScrollDetect(newModalID);
    $(".custom-goto-top").attr("onclick", `goToTop('${newModalID}')`);
  }
  
  function customCloseSecondModal(currentModalID, prevModalID) {
    $(`${currentModalID}`).removeAttr("style");
    $(`${currentModalID}`).removeClass("show");
    $(`${currentModalID}`).removeAttr("style");
  
    $(`${prevModalID}`).addClass("show");
    $(`${prevModalID}`).css("display", "block");
    $(`${prevModalID}`).css("background-color", "rgba(0, 0, 0, 0.5)");
  
    // 取消綁定 goToTopScroll 事件
    $(`${currentModalID}`).unbind();
    // 綁定 goToTopScroll 事件
    // customScrollDetect(prevModalID);
    // goToTopScroll(prevModalID);
    
    $(".custom-goto-top").attr("onclick", `goToTop('${prevModalID}')`);
  }
  