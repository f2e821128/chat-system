$(document).ready(async function () {
  document.querySelector(`#flatpickr-range-input-01`).flatpickr(flatpickr_range_config);

  /**
   * On Mobile/Pad mode click chat list display chat content.
   */
  $('a[data-chat="content-num"]').click(() => {
    $('main[data-dropzone-area=""]').addClass("is-visible");
  });

  /**
   * On Mobile/Pad mode goback arrow.
   */
  $("a[data-toggle-chat]").click(() => {
    $('main[data-dropzone-area=""]').removeClass("is-visible");
  });

  /**
   * detect window screen onresize for CSS-IN-JS
   */
  $(window).resize(function () {
    window.innerWidth <= 1024 ? $("#offcanvas-more-group").removeClass("show") : $("#offcanvas-more-group").addClass("show");
  });

  /**
   * detect window screen for CSS-IN-JS
   */
  let resizeObserver = new ResizeObserver(() => {
    window.innerWidth <= 1024 ? $("#offcanvas-more-group").removeClass("show") : $("#offcanvas-more-group").addClass("show");
  });
  // Add a listener to body
  resizeObserver.observe(document.getElementsByTagName("body")[0]);
});
