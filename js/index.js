// Drill Down Config
function highChart01() {
  const DRILLDOWNARR = ["drilldown-container-01", "drilldown-container-02"];
  DRILLDOWNARR.forEach((id) => {
    Highcharts.chart(`${id}`, {
      chart: {
        type: "column",
      },
      title: {
        align: "left",
        text: "Browser market shares. January, 2022",
      },
      subtitle: {
        align: "left",
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>',
      },
      accessibility: {
        announceNewData: {
          enabled: true,
        },
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        title: {
          text: "Total percent market share",
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y:.1f}%",
          },
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>',
      },
      series: [
        {
          name: "Browsers",
          colorByPoint: true,
          data: [
            {
              name: "Chrome",
              y: 63.06,
              drilldown: "Chrome",
            },
            {
              name: "Safari",
              y: 19.84,
              drilldown: "Safari",
            },
            {
              name: "Firefox",
              y: 4.18,
              drilldown: "Firefox",
            },
            {
              name: "Edge",
              y: 4.12,
              drilldown: "Edge",
            },
            {
              name: "Opera",
              y: 2.33,
              drilldown: "Opera",
            },
            {
              name: "Internet Explorer",
              y: 0.45,
              drilldown: "Internet Explorer",
            },
            {
              name: "Other",
              y: 1.582,
              drilldown: null,
            },
          ],
        },
      ],
      drilldown: {
        breadcrumbs: {
          position: {
            align: "right",
          },
        },
        series: [
          {
            name: "Chrome",
            id: "Chrome",
            data: [
              ["v65.0", 0.1],
              ["v64.0", 1.3],
              ["v63.0", 53.02],
              ["v62.0", 1.4],
              ["v61.0", 0.88],
              ["v60.0", 0.56],
              ["v59.0", 0.45],
              ["v58.0", 0.49],
              ["v57.0", 0.32],
              ["v56.0", 0.29],
              ["v55.0", 0.79],
              ["v54.0", 0.18],
              ["v51.0", 0.13],
              ["v49.0", 2.16],
              ["v48.0", 0.13],
              ["v47.0", 0.11],
              ["v43.0", 0.17],
              ["v29.0", 0.26],
            ],
          },
          {
            name: "Firefox",
            id: "Firefox",
            data: [
              ["v58.0", 1.02],
              ["v57.0", 7.36],
              ["v56.0", 0.35],
              ["v55.0", 0.11],
              ["v54.0", 0.1],
              ["v52.0", 0.95],
              ["v51.0", 0.15],
              ["v50.0", 0.1],
              ["v48.0", 0.31],
              ["v47.0", 0.12],
            ],
          },
          {
            name: "Internet Explorer",
            id: "Internet Explorer",
            data: [
              ["v11.0", 6.2],
              ["v10.0", 0.29],
              ["v9.0", 0.27],
              ["v8.0", 0.47],
            ],
          },
          {
            name: "Safari",
            id: "Safari",
            data: [
              ["v11.0", 3.39],
              ["v10.1", 0.96],
              ["v10.0", 0.36],
              ["v9.1", 0.54],
              ["v9.0", 0.13],
              ["v5.1", 0.2],
            ],
          },
          {
            name: "Edge",
            id: "Edge",
            data: [
              ["v16", 2.6],
              ["v15", 0.92],
              ["v14", 0.4],
              ["v13", 0.1],
            ],
          },
          {
            name: "Opera",
            id: "Opera",
            data: [
              ["v50.0", 0.96],
              ["v49.0", 0.82],
              ["v12.1", 0.14],
            ],
          },
        ],
      },
    });
  });
}

// Basic Bar Chart
function highChart02() {
  const BARCHARTSARR = ["barchart-container-01", "barchart-container-02"];

  BARCHARTSARR.forEach((id) => {
    Highcharts.chart(`${id}`, {
      chart: {
        type: "column",
      },
      title: {
        text: "Monthly Average Rainfall",
      },
      subtitle: {
        text: "Source: WorldClimate.com",
      },
      xAxis: {
        categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Rainfall (mm)",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Tokyo",
          data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
        },
      ],
    });
  });
}

// Line Chart
function highChart03() {
  Highcharts.chart("container-03", {
    title: {
      text: "U.S Solar Employment Growth by Job Category, 2010-2020",
      align: "left",
    },

    subtitle: {
      text: 'Source: <a href="https://irecusa.org/programs/solar-jobs-census/" target="_blank">IREC</a>',
      align: "left",
    },

    yAxis: {
      title: {
        text: "Number of Employees",
      },
    },

    xAxis: {
      accessibility: {
        rangeDescription: "Range: 2010 to 2020",
      },
    },

    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false,
        },
        pointStart: 2010,
      },
    },

    series: [
      {
        name: "Installation & Developers",
        data: [43934, 48656, 65165, 81827, 112143, 142383, 171533, 165174, 155157, 161454, 154610],
      },
      {
        name: "Manufacturing",
        data: [24916, 37941, 29742, 29851, 32490, 30282, 38121, 36885, 33726, 34243, 31050],
      },
      {
        name: "Sales & Distribution",
        data: [11744, 30000, 16005, 19771, 20185, 24377, 32147, 30912, 29243, 29213, 25663],
      },
      {
        name: "Operations & Maintenance",
        data: [null, null, null, null, null, null, null, null, 11164, 11218, 10077],
      },
      {
        name: "Other",
        data: [21908, 5548, 8105, 11248, 8989, 11816, 18274, 17300, 13053, 11906, 10073],
      },
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
            },
          },
        },
      ],
    },
  });
}

$(document).ready(async function () {
  document.querySelector(`#flatpickr-range-input-01`).flatpickr(flatpickr_datetime_config);
  document.querySelector(`#flatpickr-range-input-02`).flatpickr(flatpickr_datetime_config);
  document.querySelector(`#flatpickr-range-input-03`).flatpickr(flatpickr_range_w_week_config);

  highChart01();
  highChart02();
  highChart03();
});
