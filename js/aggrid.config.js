// Custom Tooltips UI Redesign.
class CustomTooltip {
  init(params) {
    const eGui = (this.eGui = document.createElement("div"));
    eGui.classList.add("custom-tooltip");

    const valueToDisplay = params.value.value ? params.value.value : "- Missing -";

    eGui.innerHTML = `
          <div class="card m-4 shadow" style="max-width: 250px">
            <div class="card-body p-6">
              <span class"text-black">${valueToDisplay}</span>
            </div>
          </div>`;
  }

  getGui() {
    return this.eGui;
  }
}

const customDateFilterParams = {
  // provide comparator function
  comparator: (filterLocalDateAtMidnight, cellValue) => {
    const dateAsString = cellValue;

    if (dateAsString == null) {
      return 0;
    }

    // In the example application, dates are stored as yyyy/mm/dd
    // We create a Date object for comparison against the filter date
    const dateParts = dateAsString?.split("/");
    const year = Number(dateParts[0]);
    const month = Number(dateParts[1]) - 1;
    const day = Number(dateParts[2]);
    const cellDate = new Date(year, month, day);

    // Now that both parameters are Date objects, we can compare
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    } else if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
    return 0;
  },
};

const customTextEditorConfig = {
  maxLength: "500", // override the editor defaults
  cols: "35",
  rows: "10",
};

const customBoolSelectConfig = {
  values: [true, false],
};

// Arrow Function
const getSearchString = ({ value }) => {
  return value;
};

const DataTableRenderer = (params) => {
  // return getMedalString(params.value);
  return params.value;
};

const toolTipValueGetter = (params) => ({ value: params.value });

const checkShowingColumns = (colDefine) => {
  const showColumns = window.localStorage.getItem("custom-showing-column")?.split(",") || [];
  if (showColumns.length > 0) {
    colDefine.forEach((object) => {
      object.hide = !showColumns.includes(object.field.toLowerCase());
    });
  }
  return colDefine;
};

// Object
const dateFilterParams = {
  comparator: (filterLocalDateAtMidnight, cellValue) => {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString?.split("/");
    var cellDate = new Date(Number(dateParts[0]), Number(dateParts[1]) - 1, Number(dateParts[1]));

    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }

    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }

    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
    return 0;
  },
};

const columnDefs = [
  {
    headerName: "ID",
    field: "id",
    hide: true,
    cellRenderer: DataTableRenderer,
    getQuickFilterText: (params) => {
      return getSearchString(params.value);
    },
  },
  {
    headerName: "Platform",
    field: "platform",
    minWidth: 180,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Date",
    field: "date",
    filter: "agDateColumnFilter",
    // add extra parameters for the date filter
    filterParams: customDateFilterParams,
    minWidth: 130,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Time",
    field: "time",
    filter: "agDateColumnFilter",
    minWidth: 100,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Member",
    field: "member",
    minWidth: 100,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "IP",
    field: "ip",
    minWidth: 150,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Region",
    field: "region",
    minWidth: 100,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Level",
    field: "level",
    minWidth: 100,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Category",
    field: "category",
    minWidth: 250,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Term",
    field: "term",
    minWidth: 250,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Details",
    field: "details",
    minWidth: 250, // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Case",
    field: "case",
    minWidth: 250, // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Member Remark",
    field: "memberRemark",
    editable: true,
    sortable: false,
    cellEditor: "agLargeTextCellEditor",
    cellEditorPopup: true,
    cellEditorParams: customTextEditorConfig,
    minWidth: 300,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Status",
    field: "status",
    editable: true,
    cellEditor: "agSelectCellEditor",
    sortable: false,
    cellEditorParams: customBoolSelectConfig,
    minWidth: 100,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Result",
    field: "result",
    editable: true,
    sortable: false,
    cellEditor: "agLargeTextCellEditor",
    cellEditorPopup: true,
    cellEditorParams: customTextEditorConfig,
    minWidth: 300,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Track Flow",
    field: "trackFlow",
    editable: true,
    sortable: false,
    cellEditor: "agLargeTextCellEditor",
    cellEditorPopup: true,
    cellEditorParams: customTextEditorConfig,
    minWidth: 300,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
  {
    headerName: "Reason",
    field: "reason",
    editable: true,
    sortable: false,
    cellEditor: "agLargeTextCellEditor",
    cellEditorPopup: true,
    cellEditorParams: customTextEditorConfig,
    minWidth: 300,
    // Tooltip
    tooltipComponent: CustomTooltip,
    tooltipValueGetter: toolTipValueGetter,
  },
];

const defaultColDef = {
  flex: 1,
  width: 300,
  filter: true,
  sortable: true,
  floatingFilter: false,
  resizable: true,
  suppressSizeToFit: true,
  suppressMenu: true,
};

const columnTypes = {
  nonEditableColumn: { editable: false },
  dateColumn: {
    suppressMenu: true,
  },
};

// let the grid know which columns and what data to use
const gridOptions = {
  columnDefs: checkShowingColumns(columnDefs),
  onCellValueChanged: onCellValueChanged,
  onRowValueChanged: onRowValueChanged,
  defaultColDef: defaultColDef,
  columnTypes: columnTypes,
  cacheQuickFilter: true,
  // Pagination Setting
  pagination: true,
  // paginationPageSize: 20,
  paginationAutoPageSize: true,
  // editType: "fullRow",
};

// Basic Function
// When data change will trigger below function.
function onCellValueChanged(event) {
  // console.log(event.data.id);
  // console.log("onCellValueChanged: " + event.colDef.field + " = " + event.newValue);
}

// When row data change will trigger below function.
function onRowValueChanged(event) {
  var data = event.data;
  // console.log("onRowValueChanged: (" + data.make + ", " + data.model + ", " + data.price + ", " + data.field5 + ")");
}

// setup the grid after the page has finished loading
document.addEventListener("DOMContentLoaded", function () {
  const gridDiv = document.querySelector("#myGrid");
  new agGrid.Grid(gridDiv, gridOptions);

  // do http request to get our sample data - not using any framework to keep the example self contained.
  // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
  fetch("../mock/data.json")
    .then((response) => response.json())
    .then((data) => {
      gridOptions.api.setRowData(data);
    });
});

// filter-text-box
function onFilterTextBoxChanged() {
  gridOptions.api.setQuickFilter(document.getElementById("filter-text-box").value);
}
