// Filter modal submit button function
function filterCondition() {
  const dateFilterID = ["single-date", "range-date"];

  const advFilterForm = document.querySelector("#advance-filter-form");
  const filterInputArr = advFilterForm.querySelectorAll("input[type='text']"); // get all input element inside form tag
  const filterModelObj = {};
  let dateModelObj = {};

  filterInputArr.forEach((inputElement) => {
    // Using parent element find select element
    const nearbySelect2 = inputElement.closest("div.input-group").querySelector("select.select2");
    // Using select id find selected data
    const select2Data = $(`#${nearbySelect2.id}`).select2("data");

    if (inputElement.value.trim() !== "" && !dateFilterID.includes(inputElement.id)) {
      filterModelObj[`${inputElement.name}`] = {};
      filterModelObj[`${inputElement.name}`].filterType = "text";
      filterModelObj[`${inputElement.name}`].type = `${select2Data[0].id}`;
      filterModelObj[`${inputElement.name}`].filter = `${inputElement.value}`;
    }
  });

  dateFilterID.forEach((dateElement) => {
    // Using parent element find select element
    const nearbyDateSelect2 = document.getElementById(`${dateElement}`).closest("div.input-group").querySelector("select.select2");
    // Using select id find selected data
    const select2DateData = $(`#${nearbyDateSelect2.id}`).select2("data");

    if (document.getElementById(`${dateElement}`).value.trim() !== "") {
      if (document.getElementById(`${dateElement}`).value.includes("~")) {
        let rangeDate = document.getElementById(`${dateElement}`).value.split("~");
        dateModelObj = {
          type: "inRange",
          dateFrom: rangeDate[0].trim(),
          dateTo: rangeDate[1].trim(),
        };
      } else {
        dateModelObj = {
          type: `${select2DateData[0].id}`,
          dateFrom: `${document.getElementById(`${dateElement}`).value}`,
        };
      }
    }
  });

  // Filter Text Function
  gridOptions.api.setFilterModel(filterModelObj);

  // Filter Date Function
  gridOptions.api.getFilterInstance("date").setModel(dateModelObj);

  gridOptions.api.onFilterChanged();

  // Close Filter Modal
  customCloseModal("#modal-advance-filter");
}

// Date Init for this page
const flatpickrSingleArray = ["single-date"];
const flatpickrRangeArray = ["range-date"];

$(document).ready(async function () {
  flatpickrSingleArray.forEach((elementId) => {
    flatpickr(`#${elementId}`, flatpickr_date_config); // flatpickr
  });

  flatpickrRangeArray.forEach((elementId) => {
    flatpickr(`#${elementId}`, flatpickr_range_config); // flatpickr
  });
});
