const flatpickr_range_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "range", // 是否要區間
  locale: {
    rangeSeparator: " ~ ", // 更換日期區間的表達式
  },
  disableMobile: "true",
  static: true,
};

const flatpickr_date_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "single",
  disableMobile: "true",
  static: true,
};

const flatpickr_datetime_config = {
  enableTime: true, // 開啟時間選項
  dateFormat: "Y-m-d H:i", // 日期時間格式
  mode: "single",
  disableMobile: "true",
  static: true,
};

const flatpickr_range_w_day_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "range", // 是否要區間
  locale: {
    rangeSeparator: " ~ ", // 更換日期區間的表達式
  },
  static: true,
  plugins: myFlatpickrPlugins,
  ranges: {
    Today: [new Date(), new Date()],
    "1d": [moment().subtract(1, "days").toDate(), new Date()],
    "2d": [moment().subtract(2, "days").toDate(), new Date()],
    "7d": [moment().subtract(7, "days").toDate(), new Date()],
  },
  rangesOnly: true, // only show the ranges menu unless the custom range button is selected
  rangesAllowCustom: true, // adds a Custom Range button to show the calendar
  rangesCustomLabel: "Custom Range", // customize the label for the custom range button
  disableMobile: "true",
};

const flatpickr_range_w_week_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "range", // 是否要區間
  locale: {
    rangeSeparator: " ~ ", // 更換日期區間的表達式
  },
  static: true,
  plugins: myFlatpickrPlugins,
  ranges: {
    "Last Week": [moment().subtract(7, "days").toDate(), new Date()],
    "Last 2 Week": [moment().subtract(14, "days").toDate(), new Date()],
    "Last 3 Week": [moment().subtract(21, "days").toDate(), new Date()],
    "Last Month": [moment().subtract(1, "month").startOf("month").toDate(), moment().subtract(1, "month").endOf("month").toDate()],
  },
  rangesOnly: true, // only show the ranges menu unless the custom range button is selected
  rangesAllowCustom: true, // adds a Custom Range button to show the calendar
  rangesCustomLabel: "Custom Range", // customize the label for the custom range button
  disableMobile: "true",
};

const flatpickr_range_w_month_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "range", // 是否要區間
  locale: {
    rangeSeparator: " ~ ", // 更換日期區間的表達式
  },
  static: true,
  plugins: myFlatpickrPlugins,
  ranges: {
    Today: [new Date(), new Date()],
    "Last 30 Days": [moment().subtract(29, "days").toDate(), new Date()],
    "This Month": [moment().startOf("month").toDate(), moment().endOf("month").toDate()],
    "Last Month": [moment().subtract(1, "month").startOf("month").toDate(), moment().subtract(1, "month").endOf("month").toDate()],
    "Last Two Month": [moment().subtract(2, "month").startOf("month").toDate(), moment().subtract(2, "month").endOf("month").toDate()],
  },
  rangesOnly: true, // only show the ranges menu unless the custom range button is selected
  rangesAllowCustom: true, // adds a Custom Range button to show the calendar
  rangesCustomLabel: "Custom Range", // customize the label for the custom range button
  disableMobile: "true",
};
