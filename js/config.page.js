function dataColumn() {
  return fetch("../mock/data.json")
    .then((response) => response.json())
    .then((data) => {
      return Object.keys(data[0]);
    });
}

function getShowingColumns() {
  return window.localStorage.getItem("custom-showing-column")?.split(",") || [];
}

function updateShowingColumns() {
  const allCheckbox = $("input[name='column-name']:checked");
  let checkedArray = [];
  for (let element of allCheckbox) {
    checkedArray.push(element.value.toLowerCase());
  }

  window.localStorage.setItem("custom-showing-column", checkedArray);

  const saveToast = document.getElementById("saveToast");
  const toast = new bootstrap.Toast(saveToast);
  toast.show();
}

function appendAvailableDataColumn(columnKeys) {
  const showingCol = getShowingColumns();
  let htmlappendText = "";

  if (showingCol.length > 0) {
    columnKeys.forEach((key) => {
      htmlappendText += `
          <div class="col-lg-2 col-md-3 col-auto p-0">
              <div class="form-check">
                  <input class="form-check-input" name="column-name" type="checkbox" value="${key}" id="${key}" ${showingCol.includes(key.toLowerCase()) ? "checked" : ""} />
                  <label class="form-check-label text-capitalize fs-4" for="flexCheckChecked"> ${key} </label>
              </div>
          </div>`;
    });
  } else {
    columnKeys.forEach((key) => {
      htmlappendText += `
          <div class="col-lg-2 col-md-3 col-auto p-0">
              <div class="form-check">
                  <input class="form-check-input" name="column-name" type="checkbox" value="${key}" id="${key}" checked />
                  <label class="form-check-label text-capitalize fs-4" for="flexCheckChecked"> ${key} </label>
              </div>
          </div>`;
    });
  }

  $("#data-table-columns").append(htmlappendText);
}

$(document).ready(async function () {
  const columnKeys = await dataColumn();
  appendAvailableDataColumn(columnKeys);
});
